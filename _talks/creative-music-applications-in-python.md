---
abstract: There is no better time than now to be a Python developer! Python is the
  language of choice for machine learning, neural networks, signal processing, and
  algorithmic music. The goal of this talk is to introduce attendees to tools and
  projects, that could be used to build creative music applications.
duration: 30
level: All
room: PennTop North
slot: 2018-10-05 16:25:00-04:00
speakers:
- Dror Ayalon
title: Creative Music Applications in Python
type: talk
---

### Creative Music Applications in Python
We are lucky to live in times when Python is the go-to programming language for machine learning,  neural networks research, web applications, as well as for developments of generative media and digital art. Using a wide variety of Python open-source packages and RNN models, the Python community is now becoming the center stage for experimental media projects and creative applications.  
  
The goal of this talk is to introduce attendees to the increasing variety of packages and pre-trained machine learning models for audio analysis, music creation, and text generation, that is now available in Python!  
  
### Talk Overview
The talk will start with a presentation of Dror's recent award-winning project, [Soundscape](http://www.soundsca.pe) -  An online platform, that allows music lovers to record loops, and sync them with music by other people around the world automatically.  
  
We will breakdown Soundscape's building blocks and see how to use audio analysis Python packages, such as [LibROSA](https://librosa.github.io/librosa/index.html) and [madmom](http://madmom.readthedocs.io/en/latest/index.html) (Recurrent Neural Network). Furthermore, we will learn how to overcome the challenges of serving these type of applications using the [Django Web Framework](https://www.djangoproject.com/).  
  
Then, we will talk about easy ways to build similar applications using Python packages, such as [AudioOwl](https://github.com/dodiku/audioowl) and [MixingBear](https://github.com/dodiku/MixingBear), which were created recently by Dror Ayalon.  
  
![AudioOwl.py and MixingBear.py](https://s3.amazonaws.com/soundscape-images-dev/AudioOwl_MixingBear.png)
  
Following the project presentation, we will review a few other Python open-source projects and tools, such as Google Magenta's Recurrent Neural Networks for generative music, WaveGAN for generative raw audio, and textgenrnn for text generation.  
  
We will wrap-up with a few interesting projects to burst our inspiration!  
  
### About The Speaker
[Dror Ayalon](https://www.drorayalon.com) is a software engineer, product manager, and interaction designer. He researches and develops innovative music creation tools, using music information retrieval (MIR) techniques, digital signal processing (DSP), and machine learning algorithms, that will allow musicians to compose music in a variety of new ways and formats. Dror recently received his master's degree from NYU and now works full time for the Google Creative Lab in NYC.