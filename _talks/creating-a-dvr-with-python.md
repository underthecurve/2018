---
abstract: Ever get so caught up in Python code and projects that you miss your favorite
  TV show airing or streaming? Let Python solve that problem for you and grab the
  show for you so you can binge watch what you want whenever you want. To be continued...
duration: 30
level: All
room: PennTop North
slot: 2018-10-06 13:00:00-04:00
speakers:
- Michael V. Battista
title: Creating a DVR with Python
type: talk
---

If you're living with multiple people, most likely the discussion about having which shows taking up space on the DVR can cause some chaos. In this talk, learn how the BattDVR project can solve this problem through the integration of multiple concepts and libraries.

Some of the points that will be covered:

* Factory Design Pattern
* Web Scraping
* Metadata collection via APIs
* Object Oriented Design and Inheritance
* Storing project preferences locally