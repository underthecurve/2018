---
abstract: "Some have never automated tests and can\u2019t check themselves before
  they wreck themselves. Others have 1000s of tests that are flaky, duplicative, and
  slow. Wa-do-we-do? GOOD testing is hard but not impossible. Start with proven advice
  from this talk!"
duration: 30
level: All
room: PennTop North
slot: 2018-10-05 14:45:00-04:00
speakers:
- Andrew Knight
title: Egad! How Do We Start Writing (Better) Tests?
type: talk
---

Some have never automated tests and can’t check themselves before they wreck themselves. Others have 1000s of tests that are flaky, duplicative, and slow. Wa-do-we-do? GOOD testing is hard but not impossible. To show how to get it done, we will follow the story of “Amanda the Panda” (representing the everyday Pythoneer) as she jumps into testing and automation with Python.

Her journey includes:
1. How do I play “buzzword bingo” for testing?
2. Why do I need feature tests and not just unit tests?
3. What skills do I need?
4. What framework should I use?
5. How do I avoid duplicating test code?
6. How do I handle test data?
7. There are too many tests! Which ones should I automate?
8. How do I run tests in continuous integration?
9. How do I make tests reliable so they stop failing again and again?
10. How do I hire Software Engineers in Test?

All examples will be given in Python (of course).