---
abstract: "Did you know the role women have played in the Python community worldwide?
  The speaker is documenting the brilliant contributions of women & other under-represented
  people. They interview both established and emerging leaders, and will share their
  insights on what\u2019s exciting in Python in 2018."
duration: 30
level: Intermediate
room: PennTop South
slot: 2018-10-05 10:45:00-04:00
speakers:
- Elissa Shevinsky
title: 'Secret Histories: The Women of Python'
type: talk
---

Did you know the role women have played in building the Python community worldwide? The speaker interviewed both established and emerging leaders in the Python community, and will share their insights on what is most exciting in Python in 2018. This is an opportunity to hear insights from other community members, and to acknowledge their contributions.

The speaker is documenting the contributions of women and under-represented people in the Python community, and continuously updating this talk as new people and interviews are added to the slides. They are Editor of the anthology “Lean Out: The Struggle for Gender Equality in Tech and Startup Culture.”