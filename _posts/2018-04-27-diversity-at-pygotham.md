---
title: Diversity at PyGotham
date: 2018-04-28 12:30:00 -0400
attribution: https://late.am/post/2018/04/27/diversity-at-pygotham-2018.html
excerpt_separator: <!-- more -->
---

This year, for the first time, PyGotham is asking everyone who has submitted
a talk to our [Call for Proposals](https://www.papercall.io/pygotham-2018)
to fill out a brief demographic survey to help support our efforts to ensure
that PyGotham is representative of the Python community we believe in,
welcoming to all our conference attendees, and helps to broaden and advance
the conversation on diversity and representation that the wider software
development community is undertaking.

<!-- more -->

![Multi-colored crayons](/static/images/crayons.jpg)
<br/>_[Saaleha Bamjee](https://www.flickr.com/photos/saaleha/)_


# Why is Diversity Important to Us?

Without any outside influence, experience has shown us that tech conference
speakers tend to predominantly come from a single background: male career
programmers. There are exceptions, but this is the pattern we see most often
at PyGotham and other tech conferences.

At PyGotham we want to do more than reflect the current demographics of
professional developers. We want to hear from scientists and designers, from
teachers and students, from community organizers and political activists,
from everyone. Anyone with an interest in technology, be it academic,
recreational, or professional, has something interesting to give a talk
about. Often, we are too close to our work to notice what is unique,
misunderstood, or confusing about it, but with a little effort, great topics
can be found everywhere.

In order to tap into that variety of topics beyond those of interest just to
professional programmers, we have to engage with speakers outside of that
group. This is, in a word, “diversity.”


# The Diversity Survey

The organizers debated taking this step back and forth. Might collecting
this information frighten away speakers from under-represented groups,
exactly those we most want to attract? Would it spark controversy and enrage
majority members who nevertheless take every opportunity to cast themselves
as victims? Would anyone even bother to fill it out?

The argument that won the day, ultimately, is that in order to improve the
diversity of speakers at PyGotham, we first need to know something about
what kind of diversity we have. Last year, I eyeballed the list of speakers
and [guesstimated that about 23% of talks had a female
speaker](https://late.am/2017/09/25/pygotham-talk-voting-retrospective.html) (some talks
had multiple speakers, so we were probably a bit lower as a share of
speakers in general). This is comparable to women’s representation numbers
at a variety of large tech companies, but as they are all pursuing internal
improvements to their diversity and inclusion programs, we too want to aim
higher.

Of course, I don’t mean to simplify all of “diversity” or “inclusion” to
just the share of women speakers. It is the only measure on which we have
any data at all, even as unreliable as name-game guessing. It’s from this
position that we decided to launch the survey this year.


# What Information We Request

We will be asking about gender identity, ethnicity, level of speaking experience,
amount of programming experience, and age.


# How We Will Use the Results

PyGotham’s program selection process this year will be similar to last
year’s: we will begin with a round of public, anonymous voting beginning
shortly after the CFP ends in late May. During public voting in 2017, 116
people cast over 12,000 votes for the nearly 200 proposals we had. (I hope
we can count on the community to show a similar amount of engagement this
year!)

The public talk voting is anonymous. Demographic information about the
author of each proposal will not be available to public voters nor PyGotham
organizers during this time. To do so would risk introducing bias, and the
purpose of public voting is to take the pulse of likely attendees with
regards to their level of interest in the submissions we receive.

After public voting, a smaller committee of PyGotham organizers and
volunteers from the community will work together to select the 60 talks that
will make it into the final conference. The vast majority of this process is
also anonymous, and will be conducted without regard to the demographic
survey’s results.

After we have a very strong candidate talk list, we must de-anonymize the
results to ensure that a) no individual speaker has too many talk slots
(this is unfair both to that speaker and to others who, by definition, don’t
get those slots), and b) to ensure that we have the proper balance of
speaker backgrounds. We will use the survey results to ensure that among the
major axes, and for the information that we have collected, that our final
speaker lineup represents at least as much diversity as is present in our
overall submission pool.

The often frustrating reality of putting together a conference schedule is
that the committee is often forced to choose a small number of talks out of
a pool of excellent proposals. Voting helps us break the tie in one
dimension — how can we produce a conference that the attendees will enjoy
and learn from — and the demographic survey results will help us break ties
in another dimension, to favor having a more diverse rather than less
diverse speaker lineup.


# Further up the Pipeline

Even if every submitter fills out the survey, by the time we are making
individual talk decisions, it is often too late to do much to move the
needle on diversity. We can ensure that our speaker lineup is representative
of the submitter pool, but what if even that pool doesn’t live up to our
expectations for what PyGotham could be?

Building on the success of last year’s [Proposal
Brainstorming](https://www.meetup.com/nycpython/events/240192272/) workshop,
we are holding at least 4 similar events this year, with one more event
scheduled on [May 2 with NYC
PyLadies](https://www.meetup.com/NYC-PyLadies/events/249924249/). At all of
these events, attendees get a chance to work with one another to suss out
what is great and talk-worthy about each of their experience.

We are also planning to proactively reach out to a number of NYC-area tech meetups
and societies representing women, people of color, and other
under-represented groups, in order to solicit their members to participate
in our call for proposals.
