---
title: "Announcing our second keynote: Gloria W."
date: 2018-07-05 10:00:00 -0400
image: /uploads/posts/gloria-w.jpg
excerpt_separator: <!--more-->
---

Founder of PyGotham, long standing NYC based Freelance Geek<!--more-->, Gloria
has been writing code since 1986, working in Embedded Systems for AT&T and
Motorola, then moving to web development and UNIX/Linux system admin in the late
90's, only to return again to Embedded Systems two years ago.  She is addicted
to problem solving, and turned this vice into a successful freelance career,
taking her through many small and medium sized companies, government agencies,
and start-ups. She has been a Python Geek since 1999, has started and helped
with many different groups over the years.
