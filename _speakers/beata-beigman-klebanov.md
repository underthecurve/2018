---
name: Beata Beigman Klebanov
talks:
- Building a Read Aloud eBook in Python
---

Beata Beigman Klebanov is a senior scientist in ETS NLP and Speech group. She holds a PhD in Computer Science. The focus of her research is automated analysis of texts in terms of quality, complexity, figuration, genre, and other aspects. She also leads a large project dealing with assistive technology to support development of reading fluency.
