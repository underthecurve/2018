---
name: Brian Muller
talks:
- I've Been Awaiting for an URL Like You
---

Brian Muller joined The Atlantic as VP of Data and Growth in 2017.  He is also a Principal with Black Jays Investments, a New York based venture capital fund focused on seed stage start-ups in the e-commerce and advertising technology spaces.  Before that, he held the position of Director of Data Science at a number of companies, including Vox Media, LivingSocial, and Omidyar Network backed Off Grid Electric (in Tanzania).  At each of these companies, he created and grew data teams focused on optimizing sales (online and inside/outside sales teams), digital marketing, customer acquisition, ad revenue, and product development.  At LivingSocial, he helped create and implement a data-driven customer growth strategy that lead to the increase from a few thousand to almost 100 million users and over half a billion dollars in annual revenue.

He was a Co-founder and CTO of the NY Times/Graham Holdings backed start-up OpBandit, a content optimization tool for online publishers.  OpBandit served major publishers across five countries before being acquired by Vox Media in 2015.

Brian has also worked as an adviser and consultant for a variety of groups including The Washington Post, Foreign Policy, PBS, and The Carnegie Endowment.  He has an MS degree in the Biomedical Sciences, and was originally trained as a statistician and computational biologist at Johns Hopkins and the Medical University of South Carolina.