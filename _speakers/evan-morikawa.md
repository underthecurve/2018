---
name: Evan Morikawa
talks:
- Making Sense of Flask APIs A Billion Requests at a Time
---

Evan Morikawa is a senior engineer at [Nylas](https://nylas.com) building out email, contact, and calendar APIs. He’s currently in NYC starting up an [East Coast branch](https://nylas.com/jobs) of Nylas. Before Nylas, Evan was founder & CTO at Proximate, an events & social network analysis company, and was a Techstars dev in residence. He enjoys Wikipedia rabbit holes and reviewing his Moves geo-tracking data to find new places to explore next.
