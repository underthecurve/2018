---
name: Sam Agnew
talks:
- Tracking the International Space Station in Django with Redis-Queue and RQ Scheduler
---

Sam Agnew is a developer evangelist at Twilio and loves inspiring and equipping developers around the world. He particularly enjoys the New York and Boston Python communities. The only thing he finds more satisfying than writing Python is playing fast guitar solos and figuring out how old video games work.